#!/usr/bin/env bash

cd "/home/"$USER

if [ -d ".llg" ]; then
  echo "Existe"
else
  mkdir ".llg"
fi

cd ".llg/"

if [ -d "minecraft" ]; then
  rm -r "minecraft"
fi

wget https://gitlab.com/legionlinuxgamers/minecraft/-/archive/$1/minecraft-$1.zip
unzip minecraft-$1.zip
mv minecraft-$1 minecraft
rm minecraft-$1.zip
rm wget-log
