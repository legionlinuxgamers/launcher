export default {
    props: {
        name: String
    },
    template: `
        <div class="col-md-6">
            <div class="container-fluid bg-g" style="margin: 5px; padding: 5px;">
                <div class="row">
                    <div class="col-3">
                        <img src="/img/minecraft/steve.jpg" class="img-fluid">
                    </div>
                    <div class="col-9 text-center"><h4>{{name}}</h4></div>
                </div>
            </div>
        </div>
    `
};