export default {
    methods: {
        close() {
            this.$neutralino.app.exit();
        }
    },
    template: `
    <nav id="header" class="navbar navbar-expand">
        <div class="container-fluid">

            <div class="ms-auto me-auto"><h1 class="text-center">Legion Linux Gamers</h1></div>

            
            <ul class="navbar-nav">
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/img/logo.svg" class="img-fluid profile" />
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
    `
};