/* eslint-disable no-undef */
const { onMounted } = Vue;

export default {
    setup() {
        onMounted(() => {
            document.body.style.backgroundImage = 'url(../img/default/background.jpg)';
        });
    },
    template: `
    <div class="container-fluid" height="100%">
        <div class="row">
            <div class="col-md-7">
                <iframe class="social" src="https://discordapp.com/widget?id=701215302054051960&theme=dark" width="100%" height="100%"
                    allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox 
                    allow-same-origin allow-scripts">
                </iframe>
            </div>
            <div class="col-md-5">
                <iframe class="social" src="https://tgwidget.com/widget/?id=6304c957a2ef7b02a11d785b" frameborder="0" scrolling="no" 
                    horizontalscrolling="no" verticalscrolling="no" width="100%" height="100%" async>
                </iframe>
            </div>
        </div>
    </div>
    `
};