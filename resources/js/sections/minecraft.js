/* eslint-disable no-undef */
import PlayerMinecraft from '../components/minecraft/playerMinecraft.js';
import Loading from '../components/loading.js';
import NotificationError from '../components/notificationError.js';
import NotificationSucces from '../components/notificationSucces.js';

const { onMounted } = Vue;

export default {
	data() {
		return {
			maxusers: 0,
			online: 0,
			players: [],
			installed: false,
			user: 'Linuxnauta',
			localVersion: '0.0.0',
			onlineVersion: '',
			requireUpdate: false,
			isLoading: true,
			jobTitle: 'Cargando',
			succesShow: false,
			succesTitle: 'EJEMPLO',
			errorShow: false,
			errorTitle: 'EJEMPLO'
		};
	},
	setup() {
		onMounted(() => {
			document.body.style.backgroundImage = 'url(../img/minecraft/background.jpg)';
		});
	},
	async beforeMount() {
		/* Server Information */
		await this.$neutralino.os.execCommand('./.scripts/minecraft/startconfig.sh');
		const response = await fetch(`${this.$serverStatus}/minecraft/linuxitos.ddnsking.com/25565`)
			.then(response => {
				return response.json();
			})
			.catch(err => {
				this.openErrorNotify(`error "${err}" al recuperar datos del servidor`);
			});
		this.maxusers = response.data.raw.vanilla.raw.players.max;
		this.online = response.data.raw.vanilla.raw.players.online;
		this.players = response.data.players;

		/* User information */
		const data = await this.$neutralino.filesystem.readFile(`${this.$home}/.llg/config/minecraft/userdata.json`)
			.then(data => { return JSON.parse(data); })
			.catch(err => {
				this.openErrorNotify(`error "${err}" al recuperar datos de usuario`);
			});
		this.installed = data.installed;
		this.user = data.user;
		this.localVersion = data.localVersion;
		this.isLoading = false;

		/* MC information */
		const versionData = await fetch(`https://gitlab.com/api/v4/projects/26488549/repository/tags`)
			.then(response => {
				return response.json();
			})
			.catch(err => {
				this.openErrorNotify(`error "${err}" al recuperar datos del servidor`);
			});
		this.onlineVersion = versionData[0].name;

		if (this.localVersion != this.onlineVersion) {
			this.requireUpdate = true;
			this.openSuccesNotify('Hay una Actualizacion Disponible')
		}
	},
	methods: {
		saveData() {
			const userData = {
				installed: this.installed,
				user: this.user,
				localVersion: this.onlineVersion
			};
			this.$neutralino.filesystem.writeFile(`${this.$home}/.llg/config/minecraft/userdata.json`, JSON.stringify(userData));
		},
		async installAndUpdate() {
			this.isLoading = true;
			this.jobTitle = 'Instalando/Actualizando Minecraft';
			await this.$neutralino.os.execCommand(`./.scripts/minecraft/update.sh ${this.onlineVersion}`)
				.then(data => {
					this.openSuccesNotify('La instalacion/actualizacion termino correctamente');
					this.installed = true;
					this.requireUpdate = false;
					this.localVersion = this.onlineVersion;
					this.saveData();
				})
				.catch(err => {
					this.openErrorNotify(`error "${err}"`);
				});
			this.isLoading = false;
		},
		openSuccesNotify(msj) {
			this.succesShow = false;
			this.succesTitle = msj;
			this.succesShow = true;
		},
		openErrorNotify(msj) {
			this.errorShow = false;
			this.errorTitle = msj;
			this.errorShow = true;
		},
		async remove() {
			this.isLoading = true;
			this.jobTitle = 'Desinstalando Minecraft';
			await this.$neutralino.os.execCommand('./.scripts/minecraft/remove.sh')
				.then(data => {
					this.openSuccesNotify('Se desinstalo el Minecraft de tu PC');
					this.installed = false;
					this.saveData();
				})
				.catch(err => {
					this.openErrorNotify(`error "${err}" al desinstalar el juego`);
				});
			this.isLoading = false;
			this.succesTitle = 'Datos cargados correctamente'
		},
		async startGame() {
			this.isLoading = true;
			this.jobTitle = 'Jugando a Maincraft';
			this.saveData();
			await this.$neutralino.os.execCommand(`~/.llg/minecraft/start.sh ${this.user}`);
			this.isLoading = false;
		},
		openTelegram() {
			this.$neutralino.os.open('tg://join?invite=HZ9Qj6_EnmT8DUoy');
		}
	},
	template: `
        <Loading v-show="isLoading" :title="jobTitle" />
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-9 text-center"><h2 class="bg-g">Conectados</h2></div>
                            <div class="col-md-3 text-center"><h2 class="bg-g">{{online}}/{{maxusers}}</h2></div>
                            <template v-for="user of players">
                                <PlayerMinecraft :name="user.name"/>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3 bg-g p-3 mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Username</label>
                        <input type="email" class="form-control" v-model="user" placeholder="name@example.com">
                    </div>
                    <div class="mb-3 bg-g p-3 mb-3">
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary btn-minecraft" @click="installAndUpdate()" v-if="!installed" type="button">
                                Instalar <i class="fa fa-download" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-primary btn-minecraft" @click="installAndUpdate()" v-if="installed" type="button">
                                Reinstalar <i class="fa fa-refresh" aria-hidden="true"></i>
                            </button>
							<button class="btn btn-primary btn-minecraft" @click="installAndUpdate()" v-if="installed && requireUpdate" type="button">
                                Actualizar <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-primary btn-danger" @click="remove()" v-if="installed" type="button">
                                Desinstalar <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="mb-3 bg-g p-3 mb-3">
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary btn-telegram" @click="openTelegram()" type="button">
                                Soporte y Comunidad <i class="fa fa-paper-plane" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="d-grid gap-2 mt-5">
                        <button class="btn btn-primary btn-minecraft btn-play" :class="{ 'disabled': !installed }" @click="startGame()" type="button">
                            Jugar <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </button>
                    </div>
					<NotificationSucces v-if="succesShow" :title="succesTitle" />
					<NotificationError v-if="errorShow" :title="errorTitle" />
                </div>
            </div>
        </div>
    `,
	components: {
		PlayerMinecraft,
		Loading,
		NotificationError,
		NotificationSucces
	}
};